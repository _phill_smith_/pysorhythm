import fire

class Note(object):
    """
    Representation of a single note on a score
    Has pitch and duration
    """
    pitch: str
    duration: int

    def __init__(self, pitch, duration):
        super(Note, self).__init__()
        self.pitch = pitch
        self.duration = duration

    def get_note(self):
        return (self.pitch, self.duration)
        

def generate(pitches: list, durations: list):
    pitch_index = 0
    duration_index = 0
    isorhythm = []

    while True:
        isorhythm.append(
            Note(
                pitch=pitches[pitch_index],
                duration=durations[duration_index]
                )
            )
        pitch_index += 1
        duration_index += 1
        if pitch_index == len(pitches):
            pitch_index = 0
        if duration_index == len(durations):
            duration_index = 0
        if pitch_index == 0 and duration_index == 0:
            break

    return [note.get_note() for note in isorhythm]


if __name__ == "__main__":
    fire.Fire({
        "generate": generate
    })